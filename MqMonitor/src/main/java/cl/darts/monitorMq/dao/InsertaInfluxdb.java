package cl.darts.monitorMq.dao;

import java.util.concurrent.TimeUnit;

import org.influxdb.InfluxDB;
import org.influxdb.InfluxDBFactory;
import org.influxdb.dto.BatchPoints;
import org.influxdb.dto.Point;

public class InsertaInfluxdb {
	
	public void insertainfluxdb(String linea) {
		try {

			System.out.println(linea);
			String[] arr = linea.split(":");
			System.out.println("Insertando InfluxDb ");
			String databaseURL = "http://10.10.26.250:8086";
			String userName = "mquser";
			String password = "mqpassword";
			System.out.println(arr[1]+arr[2]);
			Float consumer = new Float(arr[2]);
			Float suscriber = new Float(arr[3]);
			Float mensajes = new Float(arr[1]);
	
			InfluxDB influxDB = InfluxDBFactory.connect(databaseURL, userName, password);
			String mq = "mq";
			BatchPoints batchPoints = BatchPoints.database(mq).retentionPolicy("mq").build();
			Point point = Point.measurement(arr[0]).time(System.currentTimeMillis(), TimeUnit.MILLISECONDS)
					.addField("mensajes", mensajes).addField("consumidores", consumer).addField("subscriptore", suscriber).build();
			batchPoints.point(point);
			influxDB.write(batchPoints);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void insertainfluxdbChannel(String linea) {
		try {

			System.out.println(linea);
			String[] arr = linea.split(":");
			System.out.println("Insertando InfluxDb ");
			String databaseURL = "http://10.10.26.250:8086";
			String userName = "mquser";
			String password = "mqpassword";
			System.out.println(arr[1]+arr[2]);
			Float totalmensajes = new Float(arr[2]);
			//String linea = name+":"+connName+":"+totalMsgs+":"+chlStatus;

	
			InfluxDB influxDB = InfluxDBFactory.connect(databaseURL, userName, password);
			String channel = "channel";
			BatchPoints batchPoints = BatchPoints.database(channel).retentionPolicy("channel").build();
			Point point = Point.measurement(arr[0]).time(System.currentTimeMillis(), TimeUnit.MILLISECONDS)
					.addField("conexiones", arr[1]).addField("totalmensajes", totalmensajes).addField("status", arr[3]).build();
			batchPoints.point(point);
			influxDB.write(batchPoints);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
