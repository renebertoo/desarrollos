package cl.darts.monitorMq.proceso;

import java.io.IOException;

import com.ibm.mq.MQQueueManager;
import com.ibm.mq.constants.CMQC;
import com.ibm.mq.constants.CMQCFC;
import com.ibm.mq.headers.MQDataException;
import com.ibm.mq.headers.pcf.PCFException;
import com.ibm.mq.headers.pcf.PCFMessage;
import com.ibm.mq.headers.pcf.PCFMessageAgent;

import cl.darts.monitorMq.dao.InsertaInfluxdb;
import cl.darts.monitorMq.util.Util;

public class ObtieneMetricasMq {
	
	public  void metricasChannel(PCFMessageAgent agent)
	   {
	 
	      PCFMessage   request = null;
	      PCFMessage[] responses = null;
	      String chlStatus = "Inactive"; 
	      Util util = new Util();
	      InsertaInfluxdb influxdb = new InsertaInfluxdb();
	 
	      try
	      {
	         
	         request = new PCFMessage(CMQCFC.MQCMD_INQUIRE_CHANNEL_STATUS);
	         request.addParameter(CMQCFC.MQCACH_CHANNEL_NAME, "*");
	         request.addParameter(CMQCFC.MQIACH_CHANNEL_INSTANCE_TYPE, CMQC.MQOT_CURRENT_CHANNEL);
	         request.addParameter(CMQCFC.MQIACH_CHANNEL_INSTANCE_ATTRS, 
	                              new int [] { CMQCFC.MQCACH_CHANNEL_NAME,
	                                           CMQCFC.MQCACH_CONNECTION_NAME,
	                                           CMQCFC.MQIACH_MSGS,
	                                           CMQCFC.MQCACH_LAST_MSG_DATE,
	                                           CMQCFC.MQCACH_LAST_MSG_TIME,
	                                           CMQCFC.MQIACH_CHANNEL_STATUS
	                              });
	 
	         responses = agent.send(request);
	          
	         for (int i = 0; i < responses.length; i++)
	         {
	            if ( ((responses[i]).getCompCode() == CMQC.MQCC_OK) &&
	                 ((responses[i]).getParameterValue(CMQCFC.MQCACH_CHANNEL_NAME) != null) )
	            {
	                         
	               String name = responses[i].getStringParameterValue(CMQCFC.MQCACH_CHANNEL_NAME);
	               if (name != null)
	                  name = name.trim();
	        
	               String connName = responses[i].getStringParameterValue(CMQCFC.MQCACH_CONNECTION_NAME);
	               if (connName != null)
	                  connName = connName.trim();
	            
	               String lastMsgDate = responses[i].getStringParameterValue(CMQCFC.MQCACH_LAST_MSG_DATE);
	               if (lastMsgDate != null)
	                  lastMsgDate = lastMsgDate.trim();
	                
	               String lastMsgTime = responses[i].getStringParameterValue(CMQCFC.MQCACH_LAST_MSG_TIME);
	               if (lastMsgTime != null)
	                  lastMsgTime = lastMsgTime.trim();
	 
	            
	               int totalMsgs = responses [i].getIntParameterValue(CMQCFC.MQIACH_MSGS);
	 
	            
	               chlStatus = util.getChannelStatus(responses [i].getIntParameterValue(CMQCFC.MQIACH_CHANNEL_STATUS));
	                
	               System.out.println("Name=" + name + " : Connection Name=" + connName  + " : Total Messages=" + totalMsgs +  
	                                            " : Last Message Date='" + lastMsgDate +  "' : Last Message Time='" + lastMsgTime +
	                                          "' : Status='" + chlStatus+"'");
	               
	               String linea = name+":"+connName+":"+totalMsgs+":"+chlStatus;
	               //influxdb.insertainfluxdbChannel(linea);
	           
	               
	            }
	         }
	      }
	      catch (IOException e)
	      {
	    	  System.out.println("IOException:" +e.getLocalizedMessage());
	      }
	      catch (MQDataException e)
	      {
	    	  System.out.println("MQDataException:" +e.getLocalizedMessage());
	      }
	      finally
	      {
	         try
	         {
	            if (agent != null)
	            {
	               agent.disconnect();
	               System.out.println("disconnected from agent");
	            }
	         }
	         catch (MQDataException e)
	         {
	           System.out.println("CC=" +e.completionCode + " : RC=" + e.reasonCode);
	         }
	 
	      }
	   }
	
	public  void metricasColas(PCFMessageAgent agent) throws MQDataException {
		
		  MQQueueManager qMgr = null;
	      PCFMessage   request = null;
	      PCFMessage[] responses = null;
	      InsertaInfluxdb influxdb = new InsertaInfluxdb();
		try
	     {

			 request = new PCFMessage(CMQCFC.MQCMD_INQUIRE_Q_STATUS);
	         request.addParameter(CMQC.MQCA_Q_NAME, "*");
	         request.addParameter(CMQCFC.MQIACF_Q_STATUS_TYPE, CMQCFC.MQIACF_Q_STATUS);

	         
	         request.addParameter(CMQCFC.MQIACF_Q_STATUS_ATTRS,
	                              new int [] { CMQC.MQCA_Q_NAME,
	                                           CMQC.MQIA_CURRENT_Q_DEPTH,
	                                           CMQC.MQIA_OPEN_INPUT_COUNT,
	                                           CMQC.MQIA_OPEN_OUTPUT_COUNT,
	                                           CMQCFC.MQCACF_LAST_PUT_DATE,
	                                           CMQCFC.MQCACF_LAST_PUT_TIME,
	                                           CMQCFC.MQCACF_LAST_GET_DATE,
	                                           CMQCFC.MQCACF_LAST_GET_TIME
	                                         });
	 
	         responses = agent.send(request);
	         
	        
	         
	         
	         for (int i = 0; i < responses.length; i++)
	         {
	            if ( ((responses[i]).getCompCode() == CMQC.MQCC_OK) &&
	                 ((responses[i]).getParameterValue(CMQC.MQCA_Q_NAME) != null) )
	            {
	               String name = responses[i].getStringParameterValue(CMQC.MQCA_Q_NAME);
	               if (name != null)
	                  name = name.trim();
	          
	 
	               int depth = responses[i].getIntParameterValue(CMQC.MQIA_CURRENT_Q_DEPTH);
	               int iprocs = responses[i].getIntParameterValue(CMQC.MQIA_OPEN_INPUT_COUNT);
	               int oprocs = responses[i].getIntParameterValue(CMQC.MQIA_OPEN_OUTPUT_COUNT);
	                
	               String lastPutDate = responses[i].getStringParameterValue(CMQCFC.MQCACF_LAST_PUT_DATE);
	               if (lastPutDate != null)
	                  lastPutDate = lastPutDate.trim();
	 
	               String lastPutTime = responses[i].getStringParameterValue(CMQCFC.MQCACF_LAST_PUT_TIME);
	               if (lastPutTime != null)
	                  lastPutTime = lastPutTime.trim();
	 
	               String lastGetDate = responses[i].getStringParameterValue(CMQCFC.MQCACF_LAST_GET_DATE);
	               if (lastGetDate != null)
	                  lastGetDate = lastGetDate.trim();
	 
	               String lastGetTime = responses[i].getStringParameterValue(CMQCFC.MQCACF_LAST_GET_TIME);
	               if (lastGetTime != null)
	                  lastGetTime = lastGetTime.trim();
	 
	               System.out.println("Name="+name + " : depth="+depth + " : iprocs="+iprocs+" : oprocs="+oprocs+" : lastPutDate='"+lastPutDate+"' : lastPutTime='"+lastPutTime+"' : lastGetDate='"+lastGetDate+"' : lastGetTime='"+lastGetTime+"'");
	               String linea =name+":"+depth+":"+iprocs+":"+oprocs;
	             // influxdb.insertainfluxdb(linea);
	            
	            }
	         }
	     }

	     catch (PCFException pcfe)
	     {
	       System.err.println ("PCF error: " + pcfe);
	     }

	     catch (IOException ioe)
	     {
	       System.err.println (ioe);
	     }
		finally
	      {
	         try
	         {
	            if (agent != null)
	            {
	               agent.disconnect();
	               System.out.println("disconnected from agent");
	            }
	         }
	         catch (MQDataException e)
	         {
	           System.out.println("CC=" +e.completionCode + " : RC=" + e.reasonCode);
	         }
	 
	      }
		
		
		
	}
	
	    
	  

}
