package cl.darts.monitorMq.util;

import com.ibm.mq.constants.CMQCFC;

public class Util {
	 public String getChannelStatus(int cs)
	   {
	      String chlStatus = "Inactive"; 
	 
	      switch (cs)
	      {
	         case CMQCFC.MQCHS_INACTIVE:
	            chlStatus = "Inactive";
	            break;
	         case CMQCFC.MQCHS_BINDING:
	            chlStatus = "Binding";
	            break;
	         case CMQCFC.MQCHS_STARTING:
	            chlStatus = "Starting";
	            break;
	         case CMQCFC.MQCHS_RUNNING:
	            chlStatus = "Running";
	            break;
	         case CMQCFC.MQCHS_STOPPING:
	            chlStatus = "Stopping";
	            break;
	         case CMQCFC.MQCHS_RETRYING:
	            chlStatus = "Retrying";
	            break;
	         case CMQCFC.MQCHS_STOPPED:
	            chlStatus = "Stopped";
	            break;
	         case CMQCFC.MQCHS_REQUESTING:
	            chlStatus = "Requesting";
	            break;
	         case CMQCFC.MQCHS_PAUSED:
	            chlStatus = "Paused";
	            break;
	         case CMQCFC.MQCHS_DISCONNECTED:
	            chlStatus = "Disconnected";
	            break;
	         case CMQCFC.MQCHS_INITIALIZING:
	            chlStatus = "Initializing";
	            break;
	         case CMQCFC.MQCHS_SWITCHING:
	            chlStatus = "Switching";
	            break;
	         default:
	            chlStatus = "Unknown ["+cs+"]";
	            break;
	      }
	       
	      return chlStatus;
	   }

}
