package cl.darts.monitorMq;
import java.util.Hashtable;

import javax.jms.JMSException;

import com.ibm.mq.MQException;
import com.ibm.mq.MQQueueManager;
import com.ibm.mq.constants.CMQC;
import com.ibm.mq.headers.MQDataException;
import com.ibm.mq.headers.pcf.PCFMessageAgent;

import cl.darts.monitorMq.proceso.ObtieneMetricasMq;

public class MonitorMain {

	public static void main(String[] args) throws JMSException, MQException {

		 try {
			 conectaQmanager();
		} catch (MQDataException e) {

			e.printStackTrace();
		} 
	}
	
	public static void conectaQmanager() throws MQDataException {
		//String qManager = "QM_ECOMM_QA_1";
		String qManager = "QM_RETAIL";
		//String qManager = "QM_ECOMM";
		//int port_num = 2404;
		int port_num = 2410;
		int openOptions = CMQC.MQOO_FAIL_IF_QUIESCING + CMQC.MQOO_INPUT_SHARED + CMQC.MQOO_INQUIRE ;

		try {
			Hashtable props = new Hashtable();
			props.put(CMQC.HOST_NAME_PROPERTY, "10.10.74.38");
			//props.put(CMQC.HOST_NAME_PROPERTY, "10.10.74.16");
			props.put(CMQC.PORT_PROPERTY, port_num);
			props.put(CMQC.CHANNEL_PROPERTY, "SYSTEM.DEF.SVRCONN");
			props.put(CMQC.USER_ID_PROPERTY, "mqm");
			props.put(CMQC.PASSWORD_PROPERTY, "mqm");
			MQQueueManager qMgr = new MQQueueManager(qManager, props);
			ObtieneMetricasMq metricasMq = new ObtieneMetricasMq();
			metricasMq.metricasColas(new PCFMessageAgent(qMgr));
			metricasMq.metricasChannel(new PCFMessageAgent(qMgr));
			qMgr.disconnect();
		} catch (MQException mqe) {
			System.out.println(mqe);
		}
	}


	
	
	
	



	
}
